import jwt, datetime, os
from flask import Flask, request
import mysql.connector

server = Flask(__name__)

# config
MYSQL_HOST      = os.getenv('MYSQL_HOST')
MYSQL_USER      = os.getenv('MYSQL_USER')
MYSQL_PASSWORD  = os.getenv('MYSQL_PASSWORD')
MYSQL_DB        = os.getenv('MYSQL_DB')
MYSQL_PORT      = os.getenv('MYSQL_PORT')

try:
    connection = mysql.connector.connect(
        host=MYSQL_HOST,
        port=int(MYSQL_PORT),
        user=MYSQL_USER,
        password=MYSQL_PASSWORD,
        database=MYSQL_DB
    )
    print("connected successfully")
except Exception as err: 
    print("exception while connecting to databse: ", err)


@server.route("/login", methods=["POST"])
def login():

    auth = request.authorization
    if not auth:
        return "missing credentials", 401
    
    cursor = connection.cursor()
    cursor.execute(
        "SELECT email, password FROM users WHERE email=%s", (auth.username,)
    )
    user_row = cursor.fetchone()
    
    if user_row:
        email = user_row[0]
        password = user_row[1]
        
        if auth.username != email or auth.password != password:
            return "invalid credentials", 401
        else:
            return createJWT(auth.username, os.environ.get("JWT_SECRET"), True)
    else:
        return "invalid credentials", 401
    
def createJWT(username,secret, authz):
    return jwt.encode(
        {
            "username": username,
            "exp": datetime.datetime.now(tz=datetime.timezone.utc)
            + datetime.timedelta(days=1),
            "iat": datetime.datetime.utcnow(),
            "admin": authz,
        },
        secret,
        algorithm="HS256", 
    )
    
@server.route("/validate", methods=["POST"])
def validate():
    auth_header = request.headers.get("Authorization")
    if not auth_header:
        return "missing credentials", 401

    parts = auth_header.split()
    if len(parts) != 2 or parts[0].lower() != "bearer":
        return "invalid authorization header", 401

    token = parts[1]

    try:
        decoded = jwt.decode(
            token, os.getenv("JWT_SECRET"), algorithms=["HS256"]
        )
    except jwt.ExpiredSignatureError:
        return "token has expired", 401
    except jwt.InvalidTokenError as err:
        return f"not authorized: {err}", 403
    except Exception as err:
        return f"internal server error: {err}", 500
    return decoded, 200


     
if __name__ == "__main__":
    server.run(host="0.0.0.0", port=8000)