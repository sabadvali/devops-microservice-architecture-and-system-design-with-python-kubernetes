import pika, json
import pika.spec


def upload(f, fs, channel, access):
    try:
        fid = fs.put(f)
    except Exception as err:
        return f"internal server error 1: {err}", 500
    message = {
        "video_fid": str(fid),
        "mp3_fid": None,
        "username": access["username"],
    }

    try:
        if channel.is_closed:
            connection = pika.BlockingConnection(pika.ConnectionParameters("rabbitmq-cluster-ip.rabbit-namespace.svc.cluster.local"))
            channel = connection.channel()

        channel.basic_publish(
            exchange="",
            routing_key="video",
            body=json.dumps(message),
            properties=pika.BasicProperties(
                delivery_mode=pika.spec.PERSISTENT_DELIVERY_MODE
            ),
        )
    except Exception as err:
        fs.delete(fid)
        return f"internal server error 2: {err}", 500