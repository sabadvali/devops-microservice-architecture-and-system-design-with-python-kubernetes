import smtplib,json, os
from email.message import EmailMessage

def notification(message):
    try:
        message = json.loads(message)
        mp3_fid = message.get("mp3_fid")
        sender_address = os.getenv("GMAIL_ADDRESS")
        sender_password = os.getenv("GMAIL_PASSWORD")
        receiver_address = message.get("username")

        if not sender_address or not sender_password:
            return "Gmail address or password not set in environment variables"

        if not receiver_address:
            return "Receiver address not provided in the message"

        msg = EmailMessage()
        msg.set_content(f"MP3 file_id: {mp3_fid} is now ready!")
        msg["Subject"] = "MP3 Download"
        msg["From"] = sender_address
        msg["To"] = receiver_address

        with smtplib.SMTP("smtp.gmail.com", 587) as session:
            session.starttls()
            session.login(sender_address, sender_password)
            session.send_message(msg)
            print("Mail Sent")
        
    except Exception as err:
        return f"Error from notification: {err}"
