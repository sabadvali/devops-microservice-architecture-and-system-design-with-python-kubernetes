import mysql.connector
import os

def initialize_db():
    root_password = os.getenv('MYSQL_ROOT_PASSWORD')
    user_password = os.getenv('MYSQL_USER_PASSWORD')
    host          = os.getenv('MYSQL_HOST')
    port          = os.getenv('MYSQL_PORT')

    connection  = mysql.connector.connect(
        host    = host,
        port    = port,
        user    = 'root',
        password= root_password
    )

    cursor = connection.cursor()
    cursor.execute("CREATE DATABASE IF NOT EXISTS auth;")
    cursor.execute("CREATE USER IF NOT EXISTS 'auth_user'@'%' IDENTIFIED BY %s;", (user_password,))
    cursor.execute("GRANT ALL PRIVILEGES ON auth.* TO 'auth_user'@'%';")
    cursor.execute("FLUSH PRIVILEGES;")
    cursor.execute("""
    CREATE TABLE IF NOT EXISTS auth.users (
        id INT AUTO_INCREMENT PRIMARY KEY,
        email VARCHAR(255) NOT NULL UNIQUE,
        password VARCHAR(255) NOT NULL
    );
    """)

    connection.commit()
    cursor.close()
    connection.close()

if __name__ == "__main__":
    initialize_db()
